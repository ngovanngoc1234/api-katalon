import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.entity.global.GlobalVariableEntity as GlobalVariableEntity
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.assertj.core.api.Assertions as Assertions
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement as ForeachStatement
import org.json.JSONArray as JSONArray
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

'login tiki API and get token'
ResponseObject reponse = WS.sendRequest(findTestObject('Testing API/TIKI Login', [('email') : 'ngovanngoc2014@gmail.com'
            , ('url') : GlobalVariable.url]))

JsonSlurper parser = new JsonSlurper()

def afterparsing = parser.parseText(reponse.getResponseBodyContent())

String accToken = afterparsing.access_token

'update info account'
ResponseObject reponseUpdateAccount = WS.sendRequest(findTestObject('Testing API/TIKI Edit Info', [('url') : GlobalVariable.url
            , ('token') : accToken]))

def afterparsingUpdateAccount = parser.parseText(reponseUpdateAccount.getResponseBodyContent())

'verify the status code'
Assertions.assertThat(reponseUpdateAccount.getStatusCode()).isEqualTo(200)

'verify account id'
Assertions.assertThat(afterparsingUpdateAccount.id).isEqualTo(14925649)

'verify account email'
Assertions.assertThat(afterparsingUpdateAccount.email).isEqualTo('ngovanngoc2014@gmail.com')

'get address to account'
ResponseObject reponseGetListAddress = WS.sendRequest(findTestObject('Testing API/TIKI Get Address', [('token') : accToken]))

def afterparsingListAddress = parser.parseText(reponseGetListAddress.getResponseBodyContent())

JSONArray arrayJson = afterparsingListAddress.data

println('list reponse = ' + arrayJson.toString())

for (int i = 0; i < arrayJson.length(); i++) {
    println('created_at = ' + arrayJson.get(i).created_at)
}

'verify the status code'
Assertions.assertThat(reponseGetListAddress.getStatusCode()).isEqualTo(200)

'Test case Create new Address and verify Address create new'
ResponseObject reponseCreateNewAddress = WS.sendRequest(findTestObject('Testing API/TIKI Create New Address', [('token') : accToken]))

def afterparsingcreateAddress = parser.parseText(reponseCreateNewAddress.getResponseBodyContent())

String id = afterparsingcreateAddress.id

'verify the status code'
Assertions.assertThat(reponseCreateNewAddress.getStatusCode()).isEqualTo(200)

'delete address'
ResponseObject reponseDeleteAddress = WS.sendRequest(findTestObject('Testing API/TIKI DeleteAddress', [('addressID') : id
            , ('token') : accToken]))

def afterparsingDeleteAddress = parser.parseText(reponseDeleteAddress.getResponseBodyContent())

'verify the status code'
Assertions.assertThat(reponseDeleteAddress.getStatusCode()).isEqualTo(200)